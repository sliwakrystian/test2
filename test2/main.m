//
//  main.m
//  test2
//
//  Created by Krystian Śliwa on 20.04.2015.
//  Copyright (c) 2015 Droids On Roids. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
