//
//  AppDelegate.h
//  test2
//
//  Created by Krystian Śliwa on 20.04.2015.
//  Copyright (c) 2015 Droids On Roids. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

